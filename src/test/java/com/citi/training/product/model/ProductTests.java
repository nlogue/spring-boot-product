package com.citi.training.product.model;

import org.junit.Test;

public class ProductTests {

	@Test
	public void testConstructor() {
		Product p = new Product(1, "Toothpaste", 1.5);
		assert(p.getId() == 1);
		assert(p.getProductName() == "Toothpaste");
		assert(p.getUnitPrice() == 1.5);
	}
	
	@Test
	public void testSettersGetters() {
		Product p = new Product(1, "Toothpaste", 1.5);
		p.setId(2);
		p.setProductName("Toothbrush");
		p.setUnitPrice(1);
		assert(p.getId() == 2);
		assert(p.getProductName() == "Toothbrush");
		assert(p.getUnitPrice() == 1);
	}

}
