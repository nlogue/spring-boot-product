package com.citi.training.product.menu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.springframework.stereotype.Component;
import com.citi.training.product.model.Product;

@Component
public class InMemoryProductRepository implements ProductRepository {
	ArrayList<Product> productList = new ArrayList<>();
	@Override
	public void saveProduct(Product product) {
		productList.add(product);
	}
	@Override
	public Product getProduct(int id) {
		return productList.get(id);
	}
	@Override
	public List<Product> getAllProducts() {
		return productList;
	}
	
	

}
