package com.citi.training.product.menu;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.citi.training.product.model.Product;

@Component
public class Demo implements ApplicationRunner {
	
	@Autowired
	ProductRepository allProducts;
	@Override
	public void run(ApplicationArguments args) throws Exception {
	
		allProducts.saveProduct(new Product(1, "Toothpaste", 1.5));
		allProducts.saveProduct(new Product(2, "Toothbrush", 0.8));
		allProducts.saveProduct(new Product(3, "Dishcloth", 0.5));
		allProducts.saveProduct(new Product(4, "Watch", 400.5));
		
		System.out.println("GET PRODUCT: " + allProducts.getProduct(2).toString());
		
		for(int i=0; i< allProducts.getAllProducts().size(); i++) {
			System.out.println(allProducts.getProduct(i).toString());
		}
		
		
	}

}
